package nfo

import (
	"encoding/xml"
	"os"

	"gitlab.com/bazzz/dates"
)

// Episode represents a TV show episode.
type Episode struct {
	XMLName xml.Name    `xml:"episodedetails"`
	Title   string      `xml:"title"`
	Season  int         `xml:"season"`
	Number  int         `xml:"episode"`
	Aired   *dates.Date `xml:"aired"`
	Plot    string      `xml:"plot"`
}

// WriteNFO writes this episode's nfo file to filepath.
func (e Episode) WriteNFO(path string) error {
	data, err := xml.MarshalIndent(e, "", "\t")
	if err != nil {
		return err
	}
	return write(path, data)
}

// Film represents a film.
type Film struct {
	XmlName   xml.Name    `xml:"movie"`
	Title     string      `xml:"title"`
	Premiered *dates.Date `xml:"premiered"`
	Overview  string      `xml:"plot"`
	Genres    []string    `xml:"genre"`
}

// WriteNFO writes this film's nfo file to path.
func (f Film) WriteNFO(path string) error {
	data, err := xml.MarshalIndent(f, "", "\t")
	if err != nil {
		return err
	}
	return write(path, data)
}

func write(path string, data []byte) error {
	result := []byte(xml.Header)
	data = append(result, data...)
	return os.WriteFile(path, data, os.ModePerm)
}
